try:
    from .path import *
except:
    from path import *
try:
    from .web import *
except ImportError:
    from .web import *

try:
    from . import amazonS3
except:
    import amazonS3

try:
    from .excel_input import *
except ImportError:
    from excel_input import *

try:
    from .excel_output import *
except ImportError:
    from excel_output import *

try:
    from .dataframe import *
except ImportError:
    from dataframe import *

try:
    from .txt_encodings import *
    from . import txt_encodings as encodings
except ImportError:
    from txt_encodings import *
    import txt_encodings as encodings

__version__ = "2.1.2"
