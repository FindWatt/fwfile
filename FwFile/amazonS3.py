import os, re
import urllib
import warnings
import boto
from boto.s3.connection import S3Connection
from boto.s3.key import Key

try:
    from .path import filename_from_path, folder_from_path
except:
    from path import filename_from_path, folder_from_path

__all__ = [
    "bucket_list",
    "get_S3_url",
    "store_file_in_S3",
    "store_file_binary",
    "retrieve_file_from_S3",
    "retrieve_file_from_S3_URL",
    "retrieve_file_from_url",
    "filenameFromS3Url",
    "delete_s3_key",
]


if not os.path.isfile(os.path.join(os.path.expanduser("~"), r".aws/credentials")):
    warnings.warn(
        """
        This module relies on the aws cli credentials being set.
        You can make sure your credentials are set by typing the
        following in your terminal:
        aws configure
        """
    )


def bucket_list(prefix="", s_bucket="pickled-models"):
    """ List pickled models in the bucket. (up to 1000 items)
        Boto2. """
    s3_conn = S3Connection()  # We're using the aws cli credentials now
    s3_bucket = s3_conn.get_bucket(s_bucket)
    s3_conn.close()
    return [
        f.key for f in s3_bucket.list(prefix=prefix)
    ]  # return result set of bucket items


def get_S3_url(s_filename, s_bucket="pickled-models", b_secure=True):
    """ List pickled models in the bucket. (up to 1000 items)
        Boto2. """
    # s3_conn = S3Connection(access_key_id, secret_access_key)
    s3_conn = S3Connection()
    s3_bucket = s3_conn.get_bucket(s_bucket)
    o_key = Key(s3_bucket)
    o_key.key = s_filename
    return o_key.generate_url(expires_in=9999999, method="GET")


def file_exists(key, bucket):
    """ Checks if a given key exists in a particular bucket"""
    # s3_conn = S3Connection(access_key_id, secret_access_key)
    s3_conn = S3Connection()
    s3_bucket = s3_conn.get_bucket(bucket)
    result = s3_bucket.get_key(key)
    return result is not None


def store_file_in_S3(
    s_path, s_filename="", s_bucket="pickled-models", headers={}, **kwargs
):
    """ Store a file in S3. Defaults to pickled_models bucket.
        Returns URL. """
    s3_conn = S3Connection()
    s3_bucket = s3_conn.get_bucket(s_bucket)

    o_key = Key(s3_bucket)
    if len(s_filename) > 0:
        o_key.key = s_filename
    else:
        o_key.key = filename_from_path(s_path)
    o_key.set_contents_from_filename(s_path, headers=headers)
    s3_conn.close()
    return o_key.generate_url(expires_in=9999999, method="GET")


def store_file_binary(f_content, key, s_bucket="pickled-models", headers={}, **kwargs):
    """ Stores binary content on S3. Returns URL """
    s3_conn = S3Connection()
    s3_bucket = s3_conn.get_bucket(s_bucket)

    o_key = Key(s3_bucket)
    o_key.key = key
    o_key.set_contents_from_string(f_content, headers=headers)
    s3_conn.close()
    return o_key.generate_url(expires_in=9999999, method="GET")


def get_file_content(s_path, s_bucket="pickled_models"):
    """ Retrieves the binary content of a file stored in S3 """
    s3_conn = S3Connection()
    s3_bucket = s3_conn.get_bucket(s_bucket)

    o_key = Key(s3_bucket)
    o_key.key = s_path
    return o_key.get_contents_as_string()


def retrieve_file_from_S3(
    s_path, s_filename="", s_bucket="pickled-models", b_unzip=True
):
    """ Download a file from S3. Defaults to pickled_models bucket,
        Returns local path. """
    # s3_conn = S3Connection(access_key_id, secret_access_key)
    s3_conn = S3Connection()
    s3_bucket = s3_conn.get_bucket(s_bucket)

    s_local_folder = folder_from_path(s_path)
    s_local_filename = filename_from_path(s_path)
    if len(s_local_filename) == 0:
        s_local_filename = s_filename

    o_key = Key(s3_bucket)
    if len(s_local_filename) == 0:
        return None
    elif len(s_filename) > 0:
        o_key.key = s_filename
    else:
        o_key.key = s_local_filename

    o_key.get_contents_to_filename(s_path)
    s3_conn.close()
    return s_local_folder + s_local_filename


def retrieve_file_from_S3_URL(s_url, s_path):
    """ Download a file in S3.
        Pass in full URL and a local path to save file.
        If filename is not included in local path, it will use the filename from URL.
        Returns local path. """

    s_site, s_bucket, s_filename = s_url.rsplit(
        "/", 2
    )  # Python2's rplsit doesn't take kwargs

    s_bucket = s_bucket.replace(
        ".s3.amazonaws.com", ""
    )  # The previous line still includes this stuff in bucket, so we're removing it
    if ":" in s_bucket:
        s_bucket = s_bucket[
            : s_bucket.index(":")
        ]  # If the url includes :port_number, lets take that out
    s_filename = filenameFromS3Url(
        s_url
    )  # Filename was keeping all of the url-signing info

    s_local_folder = folder_from_path(s_path)
    s_local_filename = filename_from_path(s_path)
    if len(s_local_filename) == 0:
        s_local_filename = s_filename

    # s3_conn = S3Connection(access_key_id, secret_access_key)
    s3_conn = S3Connection()
    s3_bucket = s3_conn.get_bucket(s_bucket)

    o_key = Key(s3_bucket)
    if len(s_filename) > 0:
        o_key.key = s_filename
        o_key.get_contents_to_filename(s_local_folder + s_local_filename)
    return s_local_folder + s_local_filename


def retrieve_file_from_url(s_url, s_path, b_unzip=True):
    """ Download a file. Return local path. """
    s_site, s_filename = s_url.rsplit(sep="/", maxsplit=1)
    s_local_folder = folder_from_path(s_path)
    s_local_filename = filename_from_path(s_path)
    if len(s_local_filename) == 0:
        s_local_filename = s_filename

    urllib.request(s_url, s_local_folder + s_local_filename)
    return s_local_folder + s_local_filename


def filenameFromS3Url(url):
    """ Obtain a filename from a S3 url """
    filename_patterns = ["com\/.*\.{}", "com\:[0-9]+\/.*\.{}"]
    file_extensions = ["zip", "rar", "pkl"]
    for filename_pattern in filename_patterns:
        for extension in file_extensions:
            pattern = filename_pattern.format(extension)
            match = re.search(pattern, url)
            if match:
                match = match.group()
                return match[match.index("/") + 1 :]


def delete_s3_key(s3_key, bucket_name):
    """
    @decription: Deletes a key from a s3 bucket.
    @arg s3_key: {str} the name of the key to delete.
    @arg bucket_name: {str} the bucket to delete the key from
    """
    print("fwutil_key: {}".format(s3_key))
    print("fwutil_bucket: {}".format(bucket_name))
    conn = S3Connection()
    bucket = conn.get_bucket(bucket_name)
    bucket.delete_key(s3_key)
