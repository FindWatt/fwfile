import sys

__all__ = ["concatenate_df_columns"]


def concatenate_df_columns(df, list_of_columns):
    """
    @description: concatenates the content (as string) of several columns of a dataframe.
    @arg df: {pandas.DataFrame} the dataframe containing the columns we want to concatenate.
    @arg list_of_columns: {list} a list of the names of the columns to concatenate
    @return: {pandas.DataFrame} a dataframe containing a column, resulting of concatenating
    the selected columns.
    """
    if sys.version_info.major < 3:
        return df[list_of_columns].apply(lambda x: " ".join(x.astype(unicode)), axis=1)
    else:
        return df[list_of_columns].apply(lambda x: " ".join(x.astype(str)), axis=1)
