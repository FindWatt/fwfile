import os, json
from collections import Counter
import pandas as pd
import openpyxl

try:
    from . import path
except:
    import path

__all__ = ["get_skip_row", "SpreadsheetInput", "MultiSheetFormatReader"]


def get_skip_row(o_excel_file, sheet_name):
    ''' Figure out how many empty rows to skip at the beginning of a pandas sheet
        by loading first few rows
        Arguments:
            o_excel_file: {pandas.io.excel.ExcelFile}
            sheet_name: sheet to analyze
        Returns:
            {int} how many rows to use in skip_rows argument of parse
    '''
    i_header = 0
    df = o_excel_file.parse(sheet_name=sheet_name, na_values=None, nrows=10, skiprows=0)
    c_cols = set([re.sub(r"[^a-z]", "", col.lower()) for col in df.columns])
    while c_cols == {'unnamed'}:
        i_header += 1
        df = o_pd.parse(sheet_name=s_sht, na_values=None, nrows=10, skiprows=i_header)
        c_cols = set([re.sub(r"[^a-z]", "", col.lower()) for col in df.columns])
    return i_header


class SpreadsheetInput:
    """The SpreadsheetInput class is meant to take a spreadsheet as input.
    If this spreadsheet is xlsb, transform it to xlsx, using a VBscript.
    And use the resulting xlsx spredsheet to produce a pandas' dataframe."""

    def __init__(self, json_config):
        """ Construct the spreadsheet input object """

        if type(json_config) == str:
            self.config = self.loadConfigFile(
                json_config
            )  # Load configurations from json file, if it's a path
        elif type(json_config) == dict:
            self.config = (
                json_config
            )  # Just use the config dictionary, if it's a dictionary

        if not "file_name" in self.config:
            print("The config file is missing the file name")
            return

        if not "transform_to_csv" in self.config:
            self.config["transform_to_csv"] = False

        if (
            str(FwPath.ext_from_path(self.config["file_name"])).lower() == ".csv"
        ):  # If the file is already a csv
            self.df_handler = CsvInput(
                self.config
            )  # And pass the config dict to the CsvInput class. CsvInput will interface with DataframeInput for the predictions, and handle the output itself.
            self.df = self.df_handler.df

        elif not self.config[
            "transform_to_csv"
        ]:  # If we're not transforming the spreadsheet to csv
            self.input = self.verifyExtension(
                self.config["file_name"]
            )  # Get the filepath of the spreadsheet
            self.df = pd.ExcelFile(self.input).parse(
                self.config["sheet_name"]
            )  # Turn the file into a pandas dataframe

        else:  # If we're transforming the slow spreadsheet to a faster CSV
            self.input = self.transformToCsv(
                self.config["file_name"]
            )  # Get the filename of the resulting csv
            self.config[
                "file_name"
            ] = self.input  # Update the config dict with the resulting csv filepath

            self.df_handler = CsvInput(
                self.config
            )  # And pass the config dict to the CsvInput class. CsvInput will interface with DataframeInput for the predictions, and handle the output itself.
            self.df = self.df_handler.df

    def verifyExtension(self, filename):
        """If the file's extension is xlsb, transform it to xlsx, and use the transformed file. Otherwise just use the original file """
        print(filename)

        if "xlsb" in filename.lower():
            # If te file is an xlsb document, transform it, and use the transformed file
            self.convertXLSBtoXLSX(
                filename
            )  # Transform the xlsb to xlsx, using the VBScrip
            just_filename = filename.split(".")[-2]
            return (
                just_filename + ".xlsx"
            )  # And return the filename with xlsx extension

        elif "xls" in filename.lower() or "xlsx" in filename.lower():
            return filename

    def transformToCsv(self, filename):
        """ Calls the VBScript that transforms XL to CSV"""
        print("Converting to CSV.....")
        os.system(
            "cscript {} {}".format(
                os.path.join(os.path.dirname(__file__), "xl_to_csv.vbs"), filename
            )
        )
        just_filename = filename.split(".")[-2]
        return just_filename + ".csv"  # Return the filename with the .csv extension

    def convertXLSBtoXLSX(self, filename):
        """Convert the XLSB to XLSX, using the VBScript """
        print("Converting to XLSX....")
        # Call the windows scripting host, to run the VBscript, using the input file as argument
        os.system(
            "cscript {} {}".format(
                os.path.join(os.path.dirname(__file__), "xlsb_to_xlsx.vbs"), filename
            )
        )

    def loadConfigFile(self, config_file_path):
        """Returns the content of the json config file """
        with open(config_file_path, "r") as config_file:
            return json.loads(config_file.read())


def MultiSheetFormatReader(s_file):
    """ This function reads the formats from the sheets in an exel file
    Looks for column number formats, header font/background colors
    and returns them as a config dictionary suitable for the MultiSheetExporter.
    Reading column widths doesn't work yet. """
    o_wb = openpyxl.load_workbook(filename=s_file, read_only=True)
    e_configs = {}

    for s_sheet in o_wb.sheetnames:  # loop through to get the formats of each sheet
        o_ws = o_wb[s_sheet]
        for o_row in o_ws.iter_rows():
            e_headers = {o_cell.value: i_col for i_col, o_cell in enumerate(o_row)}
            e_cols = {i_col: s_header for s_header, i_col in e_headers.items()}
            break

        e_sheet_config, e_headers, e_number_formats = {}, {}, {}

        o_rows = o_ws.iter_rows()
        o_row = next(o_rows)  # header row
        e_cols = {i_col + 1: o_cell.value for i_col, o_cell in enumerate(o_row)}

        e_header_font_color = {
            e_cols[i_col + 1]: "#{}".format(o_cell.font.color.index[2:])
            for i_col, o_cell in enumerate(o_row)
            if o_cell and o_cell.font and isinstance(o_cell.font.color.index, str)
        }
        e_header_bg_color = {}
        #        try:
        #            e_header_bg_color = {
        #                e_cols[i_col + 1]:"#{}".format(o_cell.style.fill.start_color.rgb[2:])
        #                for i_col, o_cell in enumerate(o_row)
        #                if isinstance(o_cell.style.fill.start_color.rgb, str) and
        #                (o_cell.style.fill.start_color.rgb[2:] != '000000' or e_cols[i_col+1] in e_header_font_color)}
        #        except AttributeError:
        #            e_header_bg_color = {
        #                e_cols[i_col + 1]:"#{}".format(o_cell.fill.start_color.rgb[2:])
        #                for i_col, o_cell in enumerate(o_row)
        #                if isinstance(o_cell.fill.start_color.rgb, str) and
        #                (o_cell.fill.start_color.rgb[2:] != '000000' or e_cols[i_col+1] in e_header_font_color)}

        # e_column_widths = {s_header:o_ws.column_dimensions[openpyxl.utils._get_column_letter(1)].width
        #           for i_col,s_header in e_cols.items()}

        for i_col, s_header in e_cols.items():
            e_headers[s_header] = i_col

            # read the number formats from the first 10 cells
            a_col_sample_number_formats = []
            for i_row in range(2, 12):
                try:
                    a_col_sample_number_formats.append(
                        o_ws.cell(row=i_row, column=i_col).number_format
                    )
                except:
                    pass
            # find most common number format
            try:
                e_number_formats[s_header] = Counter(
                    a_col_sample_number_formats
                ).most_common(1)[0][0]
            except:
                pass

        if e_number_formats:
            e_sheet_config["col_format"] = e_number_formats
        # if e_column_widths: e_sheet_config['col_width'] = e_column_widths
        if e_header_bg_color:
            e_sheet_config["fill_color"] = e_header_bg_color
        if e_header_font_color:
            e_sheet_config["font_color"] = e_header_font_color

        e_configs[s_sheet] = e_sheet_config
    o_wb._archive.close()

    return e_configs
