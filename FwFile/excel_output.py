import os, json, re, time
import pandas as pd
import numpy as np
import xlsxwriter as xl
from io import BytesIO, StringIO
import pandas as pd
from pyexcelerate import (
    Workbook,
    Color,
    Style,
    Font,
    Fill,
    Format,
    Alignment,
    Panes,
    Worksheet,
)
from openpyxl import load_workbook
from FwUtility import secondsToStr, process_time, process_memory

try:
    from .path import get_new_write_filename, add_suffix_to_file
except:
    from path import get_new_write_filename, add_suffix_to_file

__all__ = [
    "SpreadsheetOutput",
    "MultiSheetExporter",
    "ExportWorkbook",
    "ExcelBufferFormatter",
    "ExportWorkbookFast",
]

"""
example of a styler dictionary to include with a sheet
e_styler = {"worksheet_format": {'show_index': True, 'tab_color': '#FF9900',
                                 'autofilter':False, 'consider_header_width': True}
            "CharCount":{'erase':True},
            "Empty_Values":{'erase':True},
            "MaxLength":{'erase':True},
            "EmptyValuesPercentage":{'erase':True},
            "TwoSticks":{'erase':True},
            "Null_Values":{'erase':True},
            "tblName":{'font_color':'black', 'cell_color':'#CEDFE6', 'width':12},
            "TotalRecords":{'num_format':"#,##0"}
           }
"""


class ExportWorkbookFast:
    """ Experiment for optimization of excel file writing using PyExcelerate
        This class has general formatting options for the workbook, not per sheet
    """

    def __init__(
        self,
        e_dfs,
        file="python_export.xlsx",  # file_path or bytesIO object
        header_font=(255, 255, 255),
        header_bg=(54, 96, 146),
        header_bold=True,
        header_wrap=True,
        header_row=1,
        auto_width=True,
        auto_filter=True,
        write_immediately=True,
        debug_print=False,
    ):
        self.dfs = {}
        self.header_font = Color(*header_font)
        self.header_bg = Color(*header_bg)
        self.header_bold = header_bold
        self.header_wrap = header_wrap
        self.header_row = max(header_row, 1)
        self.auto_width = auto_width
        self.auto_filter = auto_filter
        self.debug_print = debug_print

        self.file = file
        self.wb, self.sheets = None, {}

        self.wb = Workbook()

        self._create_sheets(e_dfs)

        if write_immediately:
            self.write()

    def write(self, file=None):
        if file is None:
            file = self.file
        n_start = time.time()
        if isinstance(file, str):
            self.wb.save(file)  # write to file
        else:
            self.wb._save(file)  # write to buffer

        self.dfs = {}
        self.sheets = {}
        if self.debug_print:
            print(
                "Writing file:\t{}\t{}\t{}".format(
                    secondsToStr(time.time() - n_start),
                    process_time(),
                    process_memory(),
                )
            )

    def _create_sheets(self, e_dfs):
        for name, df in e_dfs.items():
            if len(df) == 0:
                continue
            n_start = time.time()
            name = self.__validate_sheet_name(name)
            self.dfs[name] = df

            if type(df).__name__ == "DataFrame":
                sht = [[col for col in df.columns]]
                sht.extend(
                    list([output_cell_value(x) for x in row] for row in df.values)
                )
            elif isinstance(df, (list, tuple)) and isinstance(df[0], (list, tuple)):
                sht = o_sheet
            else:
                print(
                    "Can't export '{}'. Need dataframe or nested list/tuple.".format(
                        name
                    )
                )
                continue

            o_sheet = self.wb.new_sheet(name, data=sht)
            self.sheets[name] = o_sheet
            o_sheet.auto_filter=self.auto_filter

            if self.header_row > 0:
                o_sheet.set_row_style(
                    self.header_row,
                    Style(
                        font=Font(bold=self.header_bold, color=self.header_font),
                        fill=Fill(background=self.header_bg),
                        alignment=Alignment(wrap_text=self.header_wrap),
                    ),
                )
                o_sheet._panes = Panes(y=self.header_row)

            if self.auto_width:
                a_widths = self.__auto_widths(sht)
                # print(dict(zip(sht[0], a_widths)))
                for i_col in a_widths:
                    o_sheet.set_col_style(i_col, Style(size=i_col))

            if self.debug_print:
                print(
                    'Processing sheet "{}":\t{}\t{}\t{}'.format(
                        name,
                        secondsToStr(time.time() - n_start),
                        process_time(),
                        process_memory(),
                    )
                )

    def __validate_sheet_name(self, s_suggested_name):
        """ Make sure sheetname is valid and unique for excel workbooks. """
        s_cleaned = re.sub("[^A-Za-z0-9>;+@$^&(),.! _|-]", "", s_suggested_name)[:31]
        if s_cleaned in self.sheets:
            i_count = 1
            while "{} ({})".format(s_cleaned[:27], i_count) in self.sheets:
                i_count += 1
            s_cleaned = "{} ({})".format(s_cleaned[:27], i_count)
        return s_cleaned

    def __auto_widths(
        self, sht, consider_header=None, max_width=40, min_width=6, rows_to_check=1000
    ):
        """ Calculate autowidth by scanning column values and looking at length. """
        if consider_header is None:
            if len(sht) < 100:
                consider_header = True
            else:
                consider_header = False

        i_start_row = self.header_row - 1 if consider_header else self.header_row
        a_auto_widths = [min_width for i_col in range(len(sht[0]))]
        for i_col in range(len(sht[0])):
            i_max = max(
                len(str(sht[i_row][i_col]))
                for i_row in range(i_start_row, min(len(sht), rows_to_check))
            )
            a_auto_widths[i_col] = min(max(i_max, min_width), max_width)

        return a_auto_widths


class ExportWorkbook(object):
    """ Format the analysis result excel files downloaded from the System
        to improve readability, filter out unneed information, and add charts.
        Writes a new excel file. """

    def __init__(
        self,
        at_dataframes_and_stylers=[],
        s_analysis_name="",
        s_input_file="",
        s_output_file="",
        s_header_font_color="white",
        s_header_bg_color="#366092",
        debug_print=False,
    ):
        """ Pass in list containing (s_sheetname, dataframe, column stylers) for each worksheet. """
        self.dfs_and_styles = at_dataframes_and_stylers
        self.header_font_color = s_header_font_color
        self.header_bg_color = s_header_bg_color
        self.debug_print = debug_print

        if s_input_file and not s_output_file:
            if s_analysis_name:
                s_output_file = get_new_write_filename(
                    add_suffix_to_file(s_input_file, "-{}".format(s_analysis_name))
                )
            else:
                s_output_file = get_new_write_filename(s_input_file)
        else:
            if s_analysis_name:
                s_output_file = get_new_write_filename(
                    add_suffix_to_file(s_output_file, "-{}".format(s_analysis_name))
                )
        self.writer = pd.ExcelWriter(
            s_output_file,
            engine="xlsxwriter",
            options={
                "strings_to_urls": False,
                "constant_memory": False,  # THIS MUST STAY FALSE
            },
        )
        self.workbook = self.writer.book
        self.sheet_names = {}

        for s_sheet_name, pd_sheet, e_styles in self.dfs_and_styles:
            self.process_sheet(s_sheet_name, pd_sheet, e_styles)

        n_start = time.time()
        self.writer.save()
        self.writer.close()
        if self.debug_print:
            print(
                "Writing file:\t{}\t{}\t{}".format(
                    secondsToStr(time.time() - n_start),
                    process_time(),
                    process_memory(),
                )
            )

    def process_sheet(self, sheet_name="sheet", pd_sheet=None, e_styles={}):
        """ Add a worksheet to the class writer. """
        if pd_sheet is None or type(pd_sheet).__name__ != "DataFrame":
            return
        n_start = time.time()
        s_sht = self.validate_sheet_name(sheet_name, pd_sheet)
        e_sheet_styles = e_styles.get("worksheet_format", {})

        # erase undesired columns
        pd_sheet.drop(
            [
                s_col
                for s_col, e_style in e_styles.items()
                if "erase" in e_style and s_col in pd_sheet
            ],
            inplace=True,
            axis=1,
        )

        # create worksheet and write data
        pd_sheet.to_excel(
            self.writer, sheet_name=s_sht, index=e_sheet_styles.get("show_index", False)
        )
        o_ws = self.writer.sheets[s_sht]

        if e_sheet_styles.get("show_index", False):
            if type(pd_sheet.index) == pd.core.indexes.multi.MultiIndex:
                i_offset = len(pd_sheet.index.levels)
            else:
                i_offset = 1
        else:
            i_offset = 0

        # set number formats and column widths
        e_auto_widths = self.auto_widths(
            pd_sheet,
            b_consider_header=e_sheet_styles.get("consider_header_width", False),
        )

        if e_sheet_styles.get("show_index", False):
            s_col = (
                pd_sheet.columns.name if pd_sheet.columns.name else "dataframe_index"
            )
            col_format = self.workbook.add_format()
            o_ws.set_column(
                0,
                0,
                e_styles.get(s_col, {}).get("width", e_auto_widths.get(s_col, 8)),
                col_format,
            )

        for i_col, s_col in enumerate(pd_sheet.columns):
            i_col += i_offset

            if s_col in e_styles:
                e_col_style = e_styles[s_col]
            else:
                e_col_style = e_styles.get(i_col, {})

            col_format = self.workbook.add_format()
            if "num_format" in e_col_style:
                try:
                    col_format.set_num_format(e_col_style["num_format"])
                except KeyError:
                    pass

            o_ws.set_column(
                i_col, i_col, e_col_style.get("width", e_auto_widths[s_col]), col_format
            )

        tab_color = e_sheet_styles.get("tab_color")
        if tab_color:
            try:
                o_ws.set_tab_color(tab_color)
            except:
                print("Couldn't set color:", tab_color)

        # freeze header, and turn on autofilter
        if e_sheet_styles.get("freeze_panes", True) is True:
            o_ws.freeze_panes(1, 0)
        if e_sheet_styles.get("autofilter", True) is True and len(pd_sheet) > 1:
            o_ws.autofilter(0, 0, 0, len(pd_sheet.columns) - 1 + i_offset)

        # color in header cells
        for i_col, s_col in enumerate(pd_sheet.columns):
            i_col += i_offset
            if s_col in e_styles:
                e_col_style = e_styles[s_col]
            else:
                e_col_style = e_styles.get(i_col, {})

            header_format = self.workbook.add_format()
            header_format.set_color(
                e_col_style.get("font_color", self.header_font_color)
            )
            header_format.set_bg_color(
                e_col_style.get("cell_color", self.header_bg_color)
            )
            header_format.set_bold(True)
            header_format.set_align("top")
            header_format.set_text_wrap()
            o_ws.write(0, i_col, s_col, header_format)

        if e_sheet_styles.get("show_index", False) and pd_sheet.columns.name:
            header_format = self.workbook.add_format()
            header_format.set_bold(True)
            header_format.set_align("top")
            header_format.set_text_wrap()
            o_ws.write(0, 0, pd_sheet.columns.name, header_format)

        if self.debug_print:
            print(
                'Processing sheet "{}":\t{}\t{}\t{}'.format(
                    s_sht,
                    secondsToStr(time.time() - n_start),
                    process_time(),
                    process_memory(),
                )
            )

    def validate_sheet_name(self, s_suggested_name, pd_sheet):
        """ Make sure sheetname is unique and valid for excel workbooks. """
        s_cleaned = re.sub("[^A-Za-z0-9>;+@$^&(),.! _|-]", "", s_suggested_name)[:31]
        if s_cleaned in self.sheet_names:
            i_count = 1
            while "{} ({})".format(s_cleaned[:27], i_count) in self.sheet_names:
                i_count += 1
            s_cleaned = "{} ({})".format(s_cleaned[:27], i_count)
        self.sheet_names[s_cleaned] = pd_sheet
        return s_cleaned

    def auto_widths(
        self, pd_sheet, b_consider_header=False, d_max_width=40, l_min_width=6
    ):
        """ Calculate autowidth by scanning column values and looking at length. """
        e_auto_widths = {
            s_col: max(
                [len(str(val)) for val in pd_sheet[s_col].value_counts().index]
                or [l_min_width]  # In case the column was empty
            )
            for s_col in pd_sheet.columns
        }

        s_col = pd_sheet.columns.name if pd_sheet.columns.name else "dataframe_index"
        e_auto_widths.update(
            {s_col: max([len(str(val)) for val in pd_sheet.index] or [l_min_width])}
        )

        if b_consider_header:
            e_auto_widths = {
                s_col: min(d_max_width, max(l_min_width, len(s_col), i_len))
                for s_col, i_len in e_auto_widths.items()
            }
        else:
            e_auto_widths = {
                s_col: min(d_max_width, max(l_min_width, i_len))
                for s_col, i_len in e_auto_widths.items()
            }

        return e_auto_widths


class ExcelBufferFormatter(ExportWorkbook):
    """
    @description: Stylizes a workbook, and outputs a BytesIO containing the
    resulting, stylized workbook.
    """

    def __init__(
        self,
        writer,
        at_dataframes_and_stylers=[],  #: List[Tuple[str, dict]] = [],
        s_header_font_color: str = "white",
        s_header_bg_color: str = "#366092",
    ) -> None:
        """
        @arg at_dataframes_and_stylers: {list} A list of tuples. Each tuples contains
        a sheet's name, the content of the sheet, a dictionary with styling information.

        @arg s_header_font_color: {str} The color of the header's font.

        @arg s_header_bg_color: {str} The color of the header's background
        """
        self.dfs_and_styles = at_dataframes_and_stylers
        self.header_font_color = s_header_font_color
        self.header_bg_color = s_header_bg_color

        # Create a buffer to store the resulting spreadsheet in.
        # TODO: Asserting the type of writer is cleaner than these conditionals
        if writer is None:
            print("Error: no writer object")
            return None
        elif isinstance(writer, (list, tuple)):
            print(
                "Error: passed in worksheet list to writer parameter instead of writer object"
            )
            return None

        self.writer = writer

        self.workbook = self.writer.book
        self.sheet_names = {}

    def style(self):
        """
        @description: Main styling routine.

        @return: {BytesIO} A buffer containing the styled workbook.
        """
        for s_sheet_name, pd_sheet, e_styles in self.dfs_and_styles:
            self.process_sheet(s_sheet_name, pd_sheet, e_styles)


class SpreadsheetOutput:
    """The SpreadsheetOutput class is meant to take a pandas dataframe as input,
    And store it in a file, according to the parameters passed in the json_config
    argument
    """

    def __init__(self, pd_dataframe, json_config):
        """ Construct the spreadsheet output object """

        self.df = pd_dataframe

        if type(json_config) == str:
            self.config = self.loadConfigFile(
                json_config
            )  # Load configurations from json file, if it's a path
        elif type(json_config) == dict:
            self.config = (
                json_config
            )  # Just use the config dictionary, if it's a dictionary

        if not "file_name" in self.config:
            print("The config file is missing the file name")
            return False

        if "file_name" in self.config:  # If an output path was provided
            self.filename = self.config["file_name"]

            if ".csv" in str(self.filename).lower():  # And it contains .csv
                self.df.to_csv(self.filename, header=True)  # Output a csv file
            else:
                if not "sheet_name" in self.config:
                    self.config["sheet_name"] = "ggl_tax"
                self.df.to_excel(
                    self.filename,
                    sheet_name=self.config["sheet_name"],
                    engine="xlsxwriter",
                )  # Otherwise output a spreadsheet
        else:
            self.df.to_csv(
                "output.csv", header=True
            )  # If no output path was provided, use a default filename


def MultiSheetExporter(e_dataframes={}, e_config={}, s_file="python_export.xlsx"):
    """
    This function write exel file from several input dataframes with desired formatting for columns : width,
    number type and switching autofilter, text wrapping and freezing panes on/off

    :param dataframes: dictionary of data frames to write to excel file with their names
    :param e_config: a dictionary of column wise settings for columns width (as 'col_width')  and number formating ('col_format')
           where dataframes have names and columns are given as 0-based integers/header names, 'freeze_column_names' identifies if first row with
           names should be frozen and 'add_autofilter' identifies using autofilter
           like {'Sheet1':{'col_width':{1:20, 2:35}, 'col_format':{0:'#.00%', 1:'#.##$', 2:'#.00'}}}
    :return: returns nothing
    """

    if not s_file.lower().endswith(".xlsx"):
        s_file += ".xlsx"

    o_writer = pd.ExcelWriter(s_file, engine="xlsxwriter")
    wb_workbook = o_writer.book

    # set any defaults
    for s_sht, e_sht_config in e_config.items():
        if not "sht_name" in e_sht_config:
            e_sht_config = str(s_sht)

    e_sht_name_map = {}
    e_shts = {}
    for s_sht, pd_frame in e_dataframes.items():
        pd_frame.to_excel(
            o_writer,
            sheet_name=validate_sheet_name(s_sht, pd_frame, e_shts, e_sht_name_map),
            index=False,
        )

    for s_sht in o_writer.sheets:
        ws = o_writer.sheets[s_sht]
        pd_sht = e_shts[s_sht]
        if e_sht_name_map[s_sht] in e_config:

            e_sht_config = e_config[e_sht_name_map[s_sht]]

            if "col_format" in e_sht_config:
                e_col_form = {
                    get_col_index(a, list(pd_sht.columns)): b
                    for a, b in e_config[e_sht_name_map[s_sht]]["col_format"].items()
                    if get_col_index(a, list(pd_sht.columns)) != -1
                }
            else:
                e_col_form = {}

            if "col_width" in e_sht_config:
                e_col_width = {
                    get_col_index(a, list(pd_sht.columns)): b
                    for a, b in e_config[e_sht_name_map[s_sht]]["col_width"].items()
                    if get_col_index(a, list(pd_sht.columns)) != -1
                }
            else:
                e_col_width = {}

            if "font_color" in e_sht_config:
                e_col_fontcol = {
                    get_col_index(a, list(pd_sht.columns)): b
                    for a, b in e_config[e_sht_name_map[s_sht]]
                    .get("font_color", {})
                    .items()
                    if get_col_index(a, list(pd_sht.columns)) != -1
                }
            else:
                e_col_fontcol = {}

            if "fill_color" in e_sht_config:
                e_col_bgcol = {
                    get_col_index(a, list(pd_sht.columns)): b
                    for a, b in e_config[e_sht_name_map[s_sht]]
                    .get("fill_color", {})
                    .items()
                    if get_col_index(a, list(pd_sht.columns)) != -1
                }
            else:
                e_col_bgcol = {}

            for i in range(len(pd_sht.columns)):
                col_format = wb_workbook.add_format()
                try:
                    col_format.set_num_format(e_col_form[i])
                except KeyError:
                    pass
                ws.set_column(i, i, e_col_width.get(i, 8), col_format)

            if e_sht_config.get("freeze_column_names", False):
                ws.freeze_panes(1, 0)

            if e_sht_config.get("add_autofilter", False):
                ws.autofilter(0, 0, 0, len(pd_sht.columns) - 1)

            for i in range(0, len(e_dataframes[s_sht].columns)):
                header_format = wb_workbook.add_format()
                header_format.set_color(e_col_fontcol.get(i, "black"))
                header_format.set_bg_color(e_col_bgcol.get(i, "white"))
                ws.write(0, i, e_dataframes[s_sht].columns[i], header_format)

    o_writer.save()
    o_writer.close()
    return True


def get_col_index(v_col, a_columns):
    if isinstance(v_col, int) and v_col < len(
        a_columns
    ):  # change to <= if non-zero indexed
        return v_col
    else:
        if v_col in a_columns:
            return a_columns.index(v_col)
        else:
            return -1


def validate_sheet_name(
    s_suggested_name, pd_dataframe, e_existing_sheet_names={}, e_sht_name_map=None
):
    """ Make sure sheetname is valid and unique for excel workbooks. """
    s_cleaned = re.sub("[^A-Za-z0-9+@$^&(),.! _|-]", "", s_suggested_name)[:31]
    if s_cleaned in e_existing_sheet_names:
        i_count = 1
        while "{} ({})".format(s_cleaned[:27], i_count) in e_existing_sheet_names:
            i_count += 1
        s_cleaned = "{} ({})".format(s_cleaned[:27], i_count)

    e_existing_sheet_names[s_cleaned] = pd_dataframe
    if not e_sht_name_map is None:
        e_sht_name_map[s_suggested_name] = s_cleaned

    return s_cleaned


s_gray_light = "#F2F2F2"
s_green_light = "#B8FFB8"
s_red_light = "#F3DDDC"
s_blue_light = "#CEDFE6"
s_cyan = "#00FFFF"
s_yellow_medium = "#FFFF99"
s_green_medium = "#99FF99"


def header_color(s_col):
    # return the hex color for standard system columns, else light gray
    s_col = s_col.lower().strip()
    if s_col.startswith("rd :"):
        return s_yellow_medium
    if s_col.startswith("d :"):
        return s_green_medium
    if s_col in ["preparedpn", "remainder", "match"]:
        return s_red_light
    if s_col in ["productgroup", "referenceclass", "pn"]:
        return s_blue_light
    return s_gray_light


def output_cell_value(v_val):
    """ return the value of a cell to write to a sheet,
        converting empty strings and nans to None """
    if isinstance(v_val, str):
        if v_val:
            return v_val
        else:
            return None
    elif isinstance(v_val, float):
        if np.isnan(v_val):
            return None
        else:
            return v_val
    elif isinstance(v_val, int):
        return v_val
    elif isinstance(v_val, list):
        if not v_val:
            return "[]"
        # if isinstance(v_val[0], (list, tuple, set, dict)):
        v_val = [output_cell_value(val) for val in v_val]
        return "[%s]" % (", ".join(v_val))
    elif isinstance(v_val, tuple):
        if not v_val:
            return "(,)"
        # if isinstance(v_val[0], (list, tuple, set, dict)):
        v_val = [output_cell_value(val) for val in v_val]
        if len(v_val) == 1:
            return "(%s,)" % (", ".join(v_val))
        else:
            return "(%s)" % (", ".join(v_val))
    elif isinstance(v_val, set):
        if not v_val:
            return "{}"
        # if isinstance(v_val[0], (list, tuple, set, dict)):
        v_val = [output_cell_value(val) for val in v_val]
        return "{%s}" % (", ".join(v_val))
    elif isinstance(v_val, dict):
        if not v_val:
            return "{}"
        return "{%s}" % (
            ", ".join(["{}: {}".format(key, value) for key, value in v_val.items()])
        )
    else:
        return v_val
