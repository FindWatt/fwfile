"""
This module is for generic functions
"""
import os, re
import zipfile

__all__ = [
    "get_sep",
    "split_path",
    "clean_filename",
    "create_folder",
    "path_exists",
    "file_exists",
    "folder_from_path",
    "filename_from_path",
    "file_from_path",
    "ext_from_path",
    "add_suffix_to_file",
    "get_new_write_filename",
    "create_new_zip",
    "unzip_to_folder",
    "get_file_list",
    "get_pickle_files",
]


def get_sep(s_path):
    i_slash = s_path.count("/")
    i_backslash = s_path.count("\\")
    i_colon = s_path.count(":")

    if max(i_slash, i_backslash, i_colon) == 0:
        return os.path.sep
    elif i_slash >= max(i_backslash, i_colon):
        return "/"
    elif i_backslash >= max(i_slash, i_colon):
        return "\\"
    elif i_colon >= max(i_slash, i_backslash):
        return ":"
    else:
        return os.path.sep


def split_path(s_path):
    re_delimiters = list(re.finditer(r"[\\/:]", s_path))
    if not re_delimiters:
        s_folder, s_file = "", s_path
    else:
        s_folder, s_file = (
            s_path[: re_delimiters[-1].end()],
            s_path[re_delimiters[-1].end() :],
        )

    s_base, s_ext = "", ""
    if s_file:
        re_ext = re.search(r"\.[^.]+$", s_file)
        if re_ext:
            s_ext = re_ext.group(0)
            s_base = s_file[: re_ext.start()]
        else:
            s_base = s_file

    return s_folder, s_file, s_base, s_ext


def clean_filename(s_filename, s_char="_", b_remove_space=True):
    s_BAD = "\"'#%&{}\\<>*?/$!:|@"
    if b_remove_space:
        s_BAD += " "

    s_filename = re.sub(
        r"[{}]+([-_+=;.,]+)[{}]+".format(s_BAD, s_BAD), r"\1", s_filename
    )
    s_filename = re.sub(r"(^[{}]+|[{}]+$)".format(s_BAD, s_BAD), "", s_filename)
    s_filename = re.sub(r"[{}]+".format(s_BAD), s_char, s_filename)
    return s_filename


def create_folder(s_path):
    ''' Create a folder if it doesn't already exist,
        even if multiple nested folders must be created
        Arguments:
            s_path: {str} of the folder path to be created (must end in separator if last element is folder)
        Returns:
            {bool} if folder is successfully made
    '''
    if os.path.exists(s_path):
        return True
    s_drive, s_folder_path = os.path.splitdrive(s_path)
    s_folder_path, s_file = os.path.split(s_folder_path)
    s_drive, s_folder_path, s_file
    
    if os.path.altsep:
        re_path_split = re.compile(r"[{}{}]".format(re.escape(os.path.sep), re.escape(os.path.altsep)))
    else:
        re_path_split = re.compile(r"[{}]".format(re.escape(os.path.sep)))
    
    a_folders = [
        x for x in re_path_split.split(s_folder_path)
        if x]
    
    for i_len in range(1, len(a_folders) + 1):
        s_check_path = os.path.join(s_drive, os.path.sep, *a_folders[:i_len])
        if not os.path.isdir(s_check_path):
            os.mkdir(s_check_path)
        #print(s_path, os.path.isdir(s_check_path))
    
    return os.path.isdir(os.path.join(s_drive, os.path.sep, s_folder_path))


def path_exists(s_path):
    """ check if folder or file exists """
    return os.path.exists(s_path)


def file_exists(s_path):
    """ check if file exists """
    return os.path.isfile(s_path)


def folder_from_path(s_path):
    """ Return folder from path. """
    s_folder, s_file, s_base, s_ext = split_path(s_path)
    if s_folder and s_folder[-1] not in {"/", "\\", ":"}:
        s_folder += os.path.sep
    return s_folder


def filename_from_path(s_path):
    """ Return filename from path. """
    s_folder, s_file, s_base, s_ext = split_path(s_path)
    return s_file


def file_from_path(s_path):
    """ Return file without extension from path. """
    s_folder, s_file, s_base, s_ext = split_path(s_path)
    return s_base


def ext_from_path(s_path):
    """ Return file extension from path. """
    s_folder, s_file, s_base, s_ext = split_path(s_path)
    return s_ext


def add_suffix_to_file(s_path, s_suffix):
    """ Return file path with suffix added after filename and before extension. """
    s_file = "{}{}{}{}".format(
        folder_from_path(s_path),
        file_from_path(s_path),
        s_suffix,
        ext_from_path(s_path),
    )
    return s_file


def get_new_write_filename(s_path):
    """ Get a new, unoccupied filename. If a file already exists, add a number
        to the end of the filename and increment it upwards until no existing file is found.
        Then return new filename to write to."""
    if not isinstance(s_path, str):
        return ""
    s_folder = folder_from_path(s_path)
    s_file = os.path.basename(s_path)

    if not path_exists(s_folder):  # if the folder doesn't exists
        return ""
    if not file_exists(s_folder + s_file):  # if a file doesn't exist with that name
        # Filepaths were coming out doubled, when only a filename was passed to this function
        return s_folder + s_file if s_folder != s_file else s_file

    s_base, s_ext = os.path.splitext(s_file)
    i_count = 1
    while file_exists(s_folder + s_base + "-" + str(i_count) + s_ext):
        i_count += 1
    return s_folder + s_base + "-" + str(i_count) + s_ext


def create_new_zip(s_zip_path, a_files):
    """ Pass in the new zip file path and a list of file paths to insert in it. """
    # We expect a_files to be a list, but we're not validating for it
    if not isinstance(a_files, list):
        a_files = [a_files]
    with zipfile.ZipFile(s_zip_path, mode="w") as o_zip:  # could add check for append
        for s_file in a_files:
            o_zip.write(s_file, filename_from_path(s_file))
        o_zip.close()
    return (
        s_zip_path
    )  # useful if the zip file gets renamed to not replace existing file.


def unzip_to_folder(s_zip_path, s_folder):
    """ Pass in the zip file path and a folder path to extract its files to. """
    zipfile.ZipFile(s_zip_path).extractall(s_folder)


def get_file_list(s_folder, c_exts=None, b_include_subfolders=False):
    """ Pass in a folder path and a set of extensions.
        Return a list of files matching any extension as absolute paths.
        Arguments:
            s_folder: {str} folder to get files
            c_exts: {set} only include files with these extensions
            b_include_subfolders: {bool} also walk through subdirectories
        Returns:
            {list} of file paths (folder + filename)
    """
    a_files = []
    if c_exts:
        e_valid_exts = set(
            [
                s_ext if s_ext.startswith(".") else ".{}".format(s_ext)
                for s_ext in c_exts
            ]
        )

        for (dirpath, dirnames, filenames) in os.walk(s_folder):
            a_files.extend(
                [
                    os.path.join(dirpath, f)
                    for f in filenames
                    if os.path.splitext(f)[1] in e_valid_exts
                ]
            )
            if not b_include_subfolders: break
    else:
        for (dirpath, dirnames, filenames) in os.walk(s_folder):
            a_files.extend([os.path.join(dirpath, f) for f in filenames])
            if not b_include_subfolders: break
    return a_files


def get_pickle_files(s_main_pickle_file):
    """ Get a list of pickle files associated with a main pkl file. """
    s_folder = folder_from_path(s_main_pickle_file)
    s_main_file = filename_from_path(s_main_pickle_file)
    a_files = get_file_list(s_folder, [".pkl", ".npz", ".npy", ".z"])
    a_pkl_files = [s_file for s_file in a_files if s_main_file in s_file]
    return a_pkl_files
