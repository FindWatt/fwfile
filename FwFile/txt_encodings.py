﻿import codecs, io, sys, gzip, os, tarfile

__all__ = ["encoding_tester", "detect_bom"]


def detect_bom(file, default=None):
    if isinstance(file, str):
        with open(file, "rb") as f:
            raw = f.read(8)  # will read less if the file is smaller
    elif isinstance(file, (io.BufferedReader, gzip.GzipFile, tarfile.ExFileObject)):
        raw = file.read(8)  # will read less if the file is smaller
        file.seek(0)
    elif isinstance(file, io.TextIOWrapper) and os.path.isfile(file.name):
        with open(file.name, "rb") as f:
            raw = f.read(8)  # will read less if the file is smaller
        file.seek(0)
    else:
        print("Can't detect BOM. Pass in path or binary file object.")
        return default

    for enc, boms in [
        # ('utf-32', [codecs.BOM_UTF32]),
        ("utf-32-le", [codecs.BOM_UTF32_LE]),
        ("utf-32-be", [codecs.BOM_UTF32_BE]),
        ("utf-16", [codecs.BOM_UTF16]),
        ("utf-16-le", [codecs.BOM_UTF16_LE]),
        ("utf-16-be", [codecs.BOM_UTF16_BE]),
        ("utf-8", [codecs.BOM_UTF8]),
    ]:
        if any(raw.startswith(bom) for bom in boms if bom):
            return enc

    if default == "utf-8":
        return "utf-8-sig"
    return default


def encoding_tester(
    file,
    s_format="",
    a_default_formats=["utf-8", "ISO-8859-2", "Windows-1252", "cp1252", "latin_1"],
):
    a_formats = [s_format]

    s_bom_encoding = detect_bom(file)
    if not s_bom_encoding in a_formats:
        a_formats.append(s_bom_encoding)
    # print('s_bom_encoding', s_bom_encoding)

    a_formats += [
        s_default for s_default in a_default_formats if not s_default == s_format
    ]

    s_sys_encoding = sys.getdefaultencoding()
    if not s_sys_encoding in a_formats:
        a_formats.append(s_sys_encoding)

    for s_encoding in a_formats:
        if not s_encoding:
            continue
        if not s_bom_encoding and s_encoding == "utf-8":
            s_encoding = "utf-8-sig"
        try:

            if isinstance(file, str):
                with open(file, "r", encoding=s_encoding) as f:
                    for line in f:
                        pass
                return s_encoding
            elif isinstance(file, io.TextIOWrapper):
                for line in file:
                    pass
                return s_encoding
            elif isinstance(file, io.BufferedReader):
                with open(file.name, "r", encoding=s_encoding) as f:
                    for line in f:
                        pass
                return s_encoding
            elif isinstance(file, gzip.GzipFile):
                with gzip.GzipFile(file.name) as o_gz:
                    with io.TextIOWrapper(o_gz, encoding=s_encoding) as f:
                        for line in f:
                            pass
                return s_encoding
        except UnicodeDecodeError:
            # print("Couldn't read file with encoding: {}".format(s_encoding))
            s_encoding = ""
            if not isinstance(file, str):
                file.seek(0)
            pass

    print("No successful encoding found.")
    return None
