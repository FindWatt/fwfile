import os, requests
import hashlib

__all__ = ["download_file"]


def download_file(url, filename=""):
    if not url or url[:4].lower() != "http":
        return ""
    local_filename = url.split("/")[-1]
    if filename:
        s_folder, s_file = os.path.split(filename)

        b_folder = True if s_folder and os.path.isdir(s_folder) else False
        if b_folder and s_file:
            local_filename = filename
        elif b_folder and not s_file:
            local_filename = os.path.join(s_folder, local_filename)
        elif s_file:
            local_filename = s_file

    if not os.path.isfile(local_filename):
        try:
            head = requests.head(url)
            if head.status_code != 200:
                return ""
            e_head = head.headers
        except:
            return ""
        if not e_head:
            return ""
        if e_head.get("Content-Length", 0) == 0:
            return ""
        b_download = True
    else:
        b_download = False
        try:
            head = requests.head(url)
            if head.status_code != 200:
                return ""
            e_head = head.headers
        except:
            return ""
        if not e_head:
            return ""

        if isinstance(e_head, dict) and e_head:
            if e_head.get("Content-Length", 0) == 0:
                return ""
            remote_time = datetime.datetime.strptime(
                e_head.get("Last-Modified"), "%a, %d %b %Y %H:%M:%S %Z"
            )
            local_time = os.path.getmtime(local_filename)
            if time.mktime(remote_time.timetuple()) > local_time:
                # print('remote newer')
                b_download = True
            # else:
            # print('local newer')

            remote_hash = re.sub("^['\"]|['\"]$", "", e_head.get("ETag", ""))
            local_hash = hashlib.md5(open(local_filename, "rb").read()).hexdigest()

            # print(local_hash, remote_hash)
            if remote_hash and remote_hash != local_hash:
                # print('local not current')
                b_download = True
            # else:
            # print('local current')

    # print('file: {}, download: {}'.format(local_filename, b_download))
    if b_download:
        r = requests.get(url, stream=True)
        with open(local_filename, "wb") as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
    return local_filename
