Sub Main()
    'Check if the xl's filepath was passed as argument
    If WScript.Arguments.Count = 0 Then
        WScript.Echo "Missing xl file path argument"
        Exit Sub
    End If
    
    'Create the Excel object'
    Dim xlobj
    Set xlobj = CreateObject("Excel.Application")
    
    'Open the workbook'
    Dim wb
    Set wb = xlobj.Workbooks.Open( WScript.Arguments(0) )
    
    
    If WScript.Arguments.Count = 2 Then 'check if sheet index/name was passed in as well
	Dim v_sheet
        v_sheet = WScript.Arguments(1)
        On Error Resume Next
        Call wb.Worksheets(v_sheet).Activate
        On Error GoTo 0
    End If
    
    'Set the output file's name and path
    Dim output_path : output_path = wb.Path & "\"
    Dim o_name : o_name = output_path & getFilenameWithoutExtension(wb.Name) & ".csv"
    
    'Save the output file'
    xlobj.DisplayAlerts = False
    On Error Resume Next
    wb.SaveAs o_name, 6
    On Error Goto 0
    xlobj.DisplayAlerts = True
    
    'Close Excel'
    xlobj.DisplayAlerts = False
    wb.Close False
End Sub

Function getFilenameWithoutExtension(s_filename) 'Get just the filename from a file, without the extension'
    Dim i_CharPos
    i_CharPos = InStrRev(s_filename, ".")
    If i_CharPos = 0 Then
        getFilenameWithoutExtension = s_filename
    Else
        getFilenameWithoutExtension = Left(s_filename, i_CharPos - 1)
    End If
End Function

Main()
