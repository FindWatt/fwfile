import os, sys, re
import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'FwFile/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = re.findall(r"\d+\.\d+\.\d+", version_line)[0]
            
class custom_install(install):
    def run(self):
        print("This is a custom installation")
        self.install_fw_dependencies()
        install.run(self)
    
    def install_fw_dependencies(self):
        # pymssql version from pypi don't work on ubuntu18. Its devs recommend installing from github
        print("About to install FindWatt dependencies...")
        if sys.version_info.major == 2:
            os.system("pip install git+https://bitbucket.org/FindWatt/fwutility")
        elif sys.version_info.major == 3:
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fwutility")

setuptools.setup(
    name="FwFile",
    version=__version__,
    url="https://bitbucket.org/FindWatt/fwfile",

    author="FindWatt",

    description="File/Path/Data utilities",
    long_description=open('README.md').read(),

    packages = setuptools.find_packages(),
    package_data={'': ["*.pyx","*.txt"]},
    py_modules=['FwFile'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "cython",
        "pandas",
        "numpy",
        "requests",
        "boto",
        "openpyxl",
        "XlsxWriter",
        "pyexcelerate",
    ],
    cmdclass={'install': custom_install}
)


