from __future__ import absolute_import
import os, gzip
import numpy as np
import pytest
import FwFile

s_folder = os.path.dirname(__file__)
s_xlsx = os.path.join(s_folder, "fixtures", "df.xlsx")
s_csv_iso_8859_2 = os.path.join(s_folder, "fixtures", "df-iso-8859-2.csv")
s_gz_csv_iso_8859_2 = os.path.join(s_folder, "fixtures", "df-iso-8859-2.csv.gz")
s_csv_utf8 = os.path.join(s_folder, "fixtures", "df.csv")
s_gz_csv_utf8 = os.path.join(s_folder, "fixtures", "df.csv.gz")
s_csv_utf8_signed = os.path.join(s_folder, "fixtures", "df-utf8-signed.csv")
s_gz_csv_utf8_signed = os.path.join(s_folder, "fixtures", "df-utf8-signed.csv.gz")
s_csv_utf8_malformed = os.path.join(s_folder, "fixtures", "df-utf8-malformed.csv")
s_gz_csv_utf8_malformed = os.path.join(s_folder, "fixtures", "df-utf8-malformed.csv.gz")
s_csv_utf16 = os.path.join(s_folder, "fixtures", "df-utf16.csv")
s_gz_csv_utf16 = os.path.join(s_folder, "fixtures", "df-utf16.csv.gz")
s_csv_utf32 = os.path.join(s_folder, "fixtures", "df-utf32.csv")
s_gz_csv_utf32 = os.path.join(s_folder, "fixtures", "df-utf32.csv.gz")


class TestEncodingTesterPath:
    def test_iso_8859_2(self):  # test standard windows text file
        result = FwFile.encoding_tester(s_csv_iso_8859_2, "ISO-8859-2")
        assert result == "ISO-8859-2"

    def test_utf8_sig(self):  # test unsigned utf8
        result = FwFile.encoding_tester(s_csv_utf8)
        assert result == "utf-8-sig"

    def test_utf8(self):  # test signed utf8
        result = FwFile.encoding_tester(s_csv_utf8_signed)
        assert result == "utf-8"

    def test_utf8_malformed(self):  # test windows text file improperly signed as utf8
        result = FwFile.encoding_tester(s_csv_utf8_malformed)
        assert result == "ISO-8859-2"

    def test_utf16(self):  # test signed utf16
        result = FwFile.encoding_tester(s_csv_utf16)
        assert result == "utf-16"

    def test_utf32(self):  # test signed utf32
        result = FwFile.encoding_tester(s_csv_utf32)
        assert result == "utf-32-le"


class TestEncodingTextFile:
    def test_iso_8859_2(self):  # test standard windows text file
        with open(s_csv_utf8_signed, "r", encoding="ISO-8859-2") as f:
            result = FwFile.encoding_tester(f, "ISO-8859-2")
        assert result == "ISO-8859-2"

    def test_utf8(self):  # test signed utf8
        with open(s_csv_utf8_signed, "r", encoding="utf-8") as f:
            result = FwFile.encoding_tester(f)
        assert result == "utf-8"

    def test_utf8(self):  # test unsigned utf8
        with open(s_csv_utf8, "r", encoding="utf-8") as f:
            result = FwFile.encoding_tester(f)
        assert result == "utf-8-sig"

    def test_utf16(self):  # test signed utf16
        with open(s_csv_utf16, "r", encoding="utf-16") as f:
            result = FwFile.encoding_tester(f, "utf-16")
        assert result == "utf-16"

    def test_utf32(self):  # test signed utf32
        with open(s_csv_utf32, "r", encoding="utf-32") as f:
            result = FwFile.encoding_tester(f, "utf-32")
        assert result == "utf-32"


class TestEncodingBufferedReader:
    def test_iso_8859_2(self):  # test standard windows text file
        with open(s_csv_iso_8859_2, "rb") as f:
            result = FwFile.encoding_tester(f, "ISO-8859-2")
        assert result == "ISO-8859-2"

    def test_utf8_sig(self):  # test unsigned utf8
        with open(s_csv_utf8, "rb") as f:
            result = FwFile.encoding_tester(f)
        assert result == "utf-8-sig"

    def test_utf8(self):  # test signed utf8
        with open(s_csv_utf8_signed, "rb") as f:
            result = FwFile.encoding_tester(f)
        assert result == "utf-8"

    def test_utf8_malformed(self):  # test windows text file improperly signed as utf8
        with open(s_csv_utf8_malformed, "rb") as f:
            result = FwFile.encoding_tester(f)
        assert result == "ISO-8859-2"

    def test_utf16(self):  # test signed utf16
        with open(s_csv_utf16, "rb") as f:
            result = FwFile.encoding_tester(f)
        assert result == "utf-16"

    def test_utf32(self):  # test signed utf32
        with open(s_csv_utf32, "rb") as f:
            result = FwFile.encoding_tester(f)
        assert result == "utf-32-le"


class TestEncodingGzip:
    def test_iso_8859_2(self):  # test standard windows text file
        with gzip.GzipFile(s_gz_csv_iso_8859_2) as f:
            result = FwFile.encoding_tester(f, "ISO-8859-2")
        assert result == "ISO-8859-2"

    def test_utf8_sig(self):  # test unsigned utf8
        with gzip.GzipFile(s_gz_csv_utf8) as f:
            result = FwFile.encoding_tester(f)
        assert result == "utf-8-sig"

    def test_utf8(self):  # test signed utf8
        with gzip.GzipFile(s_gz_csv_utf8_signed) as f:
            result = FwFile.encoding_tester(f)
        assert result == "utf-8"

    def test_utf8_malformed(self):  # test windows text file improperly signed as utf8
        with gzip.GzipFile(s_gz_csv_utf8_malformed) as f:
            result = FwFile.encoding_tester(f)
        assert result == "ISO-8859-2"

    def test_utf16(self):  # test signed utf16
        with gzip.GzipFile(s_gz_csv_utf16) as f:
            result = FwFile.encoding_tester(f)
        assert result == "utf-16"

    def test_utf32(self):  # test signed utf32
        with gzip.GzipFile(s_gz_csv_utf32) as f:
            result = FwFile.encoding_tester(f)
        assert result == "utf-32-le"


class TestDetectBOM:
    def test_iso_8859_2(self):  # test standard windows text file
        result = FwFile.detect_bom(s_csv_iso_8859_2)
        assert result is None

    def test_utf8_sig(self):  # test signed utf8
        result = FwFile.detect_bom(s_csv_utf8_signed)
        assert result == "utf-8"

    def test_utf16(self):  # test signed utf16
        result = FwFile.detect_bom(s_csv_utf16)
        assert result == "utf-16"

    def test_utf32(self):  # test signed utf32
        result = FwFile.detect_bom(s_csv_utf32)
        assert result == "utf-32-le"

    def test_buf_iso_8859_2(self):  # test standard windows text file as opened file
        with open(s_csv_iso_8859_2, "rb") as f:
            result = FwFile.detect_bom(f)
        assert result is None

    def test_gz_iso_8859_2(self):  # test standard windows text file in gz archive
        with gzip.GzipFile(s_gz_csv_iso_8859_2) as f:
            result = FwFile.detect_bom(f)
        assert result is None

    def test_buf_utf8_signed(self):  # test signed utf8 as opened file
        with open(s_csv_utf8_signed, "rb") as f:
            result = FwFile.detect_bom(f)
        assert result == "utf-8"

    def test_gz_utf8_signed(self):  # test signed utf8 in gz archive
        with gzip.GzipFile(s_gz_csv_utf8_signed) as f:
            result = FwFile.detect_bom(f)
        assert result == "utf-8"
