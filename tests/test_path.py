﻿from __future__ import absolute_import
import os
import numpy as np
import pytest
import FwFile


class TestPathSeparator:
    def test_get_sep_blank(self):
        assert FwFile.get_sep("") == os.path.sep

    def test_get_sep_file(self):
        assert FwFile.get_sep("test.txt") == os.path.sep

    def test_get_sep_url(self):
        assert FwFile.get_sep(r"http://findwatt.com/catalogs") == "/"

    def test_get_sep_linux(self):
        assert FwFile.get_sep(r"/usr/Test/file.txt") == "/"

    def test_get_sep_windows(self):
        assert FwFile.get_sep(r"C:\Users\Test\file.txt") == "\\"

    def test_get_sep_mac(self):
        assert FwFile.get_sep(r"Users : Test : file.txt") == ":"

    def test_get_sep_mixed(self):
        assert FwFile.get_sep(r"C:\Users/Test\file.txt") == "\\"

    def test_get_sep_mixed_equal(self):
        assert FwFile.get_sep(r"C:\Users/Test/duplicate\file.txt") == "/"


class TestPathSplitter:
    def test_split_path_blank(self):
        t_path = FwFile.split_path("")
        assert t_path == ("", "", "", "")

    def test_split_path_file(self):
        t_path = FwFile.split_path("test.txt")
        assert t_path == ("", "test.txt", "test", ".txt")

    def test_split_path_base(self):
        t_path = FwFile.split_path("test")
        assert t_path == ("", "test", "test", "")

    def test_split_path_ext(self):
        t_path = FwFile.split_path(".txt")
        assert t_path == ("", ".txt", "", ".txt")

    def test_split_path_url(self):
        t_path = FwFile.split_path(r"http://findwatt.com/catalogs")
        assert t_path == ("http://findwatt.com/", "catalogs", "catalogs", "")

    def test_split_path_linux(self):
        t_path = FwFile.split_path(r"/usr/Test/file.txt")
        assert t_path == ("/usr/Test/", "file.txt", "file", ".txt")

    def test_split_path_windows(self):
        t_path = FwFile.split_path(r"C:\Users\Test\file.txt")
        assert t_path == (r"C:\Users\Test\\"[:-1], "file.txt", "file", ".txt")

    def test_split_path_mac(self):
        t_path = FwFile.split_path(r"Users:Test:file.txt")
        assert t_path == ("Users:Test:", "file.txt", "file", ".txt")

    def test_split_path_mixed(self):
        t_path = FwFile.split_path(r"C:\Users/Test\file.txt")
        assert t_path == (r"C:\Users/Test\\"[:-1], "file.txt", "file", ".txt")

    def test_split_path_mixed_equal(self):
        t_path = FwFile.split_path(r"C:\Users/Test\duplicate/file.txt")
        assert t_path == (r"C:\Users/Test\duplicate/", "file.txt", "file", ".txt")


class TestCleanFilename:
    def test_empty(self):
        assert FwFile.clean_filename("") == ""

    def test_clean(self):
        assert FwFile.clean_filename("word") == "word"

    def test_spaces(self):
        assert FwFile.clean_filename("word word") == "word_word"

    def test_spaces_allowed(self):
        assert FwFile.clean_filename("word word", b_remove_space=False) == "word word"

    def test_lead_trail(self):
        assert FwFile.clean_filename(" _ word /") == "_word"

    def test_existing_delimiter_good(self):
        assert FwFile.clean_filename("word - word") == "word-word"

    def test_existing_delimiter_bad(self):
        assert FwFile.clean_filename("word : word") == "word_word"

    def test_alt_char(self):
        assert FwFile.clean_filename("word word", s_char="-") == "word-word"


class TestPath:
    def test_path_exists(self):
        assert FwFile.path_exists(__file__)

    def test_file_exists(self):
        assert FwFile.file_exists(__file__)

    def test_folder_from_path_windows(self):
        result = FwFile.folder_from_path(r"C:\Users\Test\file.txt")
        assert result[:-1] == r"C:\Users\Test"

    def test_folder_from_path_linux(self):
        result = FwFile.folder_from_path(r"/usr/Test/file.txt")
        assert result[:-1] == r"/usr/Test"

    def test_filename_from_path_windows(self):
        result = FwFile.filename_from_path(r"C:\Users\Test\file.txt")
        assert result == r"file.txt"

    def test_filename_from_path_linux(self):
        result = FwFile.filename_from_path(r"/usr/Test/file.txt")
        assert result == r"file.txt"

    def test_file_from_path_windows(self):
        result = FwFile.file_from_path(r"C:\Users\Test\file.txt")
        assert result == r"file"

    def test_file_from_path_linux(self):
        result = FwFile.file_from_path(r"/usr/Test/file.txt")
        assert result == r"file"

    def test_ext_from_path_windows(self):
        result = FwFile.ext_from_path(r"C:\Users\Test\file.txt")
        assert result == r".txt"

    def test_ext_from_path_linux(self):
        result = FwFile.ext_from_path(r"/usr/Test/file.txt")
        assert result == r".txt"

    def test_add_suffix_to_file(self):
        result = FwFile.add_suffix_to_file(r"C:\Users\Test\file.txt", "-test")
        assert result == r"C:\Users\Test\file-test.txt"
